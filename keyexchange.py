#! /usr/bin/env python

# Andrew Lampert
# 250 643 797
# ALamper@uwo.ca

# IMPORTS
import filemanager

# VARIABLES
filepath_given_data 	= "assignment2-values.txt"
client_pvt_key 			= 250643797				# Private key = student number
client_pub_key 			= ""
server_pvt_key			= ""
server_pub_key			= ""
secret 					= ""

try:

	p 				= int(filemanager.get_file_var(filepath_given_data, "p"))	# Public Mod
	g				= int(filemanager.get_file_var(filepath_given_data, "g"))	# Public base
	server_pub_key	= int(filemanager.get_file_var(filepath_given_data, "y"))	# Assuming y is the server pub key


	# Calculating the clients public key: g^pvtKey mod p 
	client_pub_key = pow(g, client_pvt_key, p)

	# Calculating shared secret
	secret = pow(server_pub_key, client_pvt_key, p)


	# Displaying Answers
	print "Client Private Key: " + str(client_pvt_key)
	print "Client Public Key: " + str(client_pub_key)
	print "Secret: " + str(secret)

	filemanager.write_file("dhparams.txt", "===== Client Public Key =====\n\n" + "yc = " + str(client_pub_key) + "\n\n" + "===== Secret =====\n\n" "s = " + str(secret))

except Exception as error:
	print repr(error);