#! /usr/bin/env python

# Andrew Lampert
# 250 643 797
# ALamper@uwo.ca

import hashlib

# PKCS Padding Function
# Function takes in a block and a block length, then returns the block with the appropriate padding
# Padding based on instructions here:
# https://tools.ietf.org/html/rfc2313#page-6
def padding(message, block_length):

	# VARIABLES
	# EB_init		= chr(00)	# Encrypted Block first character
	# BT 			= chr(01)	# Block Type
	# PS 			= chr(00)	# Padding String
	EB_init		= "00"	# Encrypted Block first character
	BT 			= "01"	# Block Type
	PS 			= "00"	# Padding String
	EB 			= ""		# Initializing EB to be an empty string
	padding 	= ""		# Padding
	asn1_header	= ""		# ASN1 Header
	msg_hash	= ""		# Hashed message

	# Setting ASN1 Header for SHA256
	asn1_header = "3031300d060960864801650304020105000420"

	# Calculating message hash using SHA256
	msg_hash = hashlib.sha256(str(message))



	# Calc padding
	pre_pad_length = len(str(EB_init+BT+PS+asn1_header+msg_hash.hexdigest()))
	
	for i in range(0, (block_length - pre_pad_length)/2  ):
		padding += "ff"

	
	#Compiling output 

	EB = EB_init + BT + padding + PS + asn1_header + msg_hash.hexdigest()

	# Error proofing
	if len(str(EB)) != 256:
		print "pre_pad_length: " + str(pre_pad_length)
		print "Block length: " + str(block_length)
		print "String length: " + str( len( str(EB) ))
		print "EB: " + EB
		raise Exception("Length of EB did not = 256")

	return EB

def verify(msg, sigma, e, n):
	crypt = pow(sigma, e, n)

	return_val = false

	if crypt == msg:
		return_val = true
	else:
		return_val = false

	raise Exception("Incomplete verifying function")

	return return_val
